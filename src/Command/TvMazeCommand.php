<?php

namespace App\Command;



use App\Entity\Bolumler;
use App\Entity\Filmler;
use App\Entity\Kanallar;
use App\Entity\Resimler;
use App\Entity\Sezonlar;
use App\Service\Film;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

// Add the Container
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

Class TvMazeCommand extends Command {
    private $em;

    public function __construct(EntityManagerInterface $em) {
        $this->em = $em;

        // In a Command, you *must* call the parent constructor
        parent::__construct();
    }
    protected function configure()
    {
        $this->setName("tvmaze:insert")
            ->setDescription("Tvmaze.com seach bölümünden arama ile verileri tabloya ekleme yapar.")
            ->setHelp("php bin\console --search='searchedword' şeklinde kullanılır.  ")
            ->addOption("search","s",InputOption::VALUE_REQUIRED,"Aramak istediniz Kelime");
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $entityManager =  $this->em;
        $io = new SymfonyStyle($input,$output);

        $cevap = $io->confirm("Eski Kayıtlar Silinsin mi ?");

        if ($cevap=="yes")
        {

            $bodyParts = $entityManager->getRepository(Filmler::class)->findAll();
            $response = array();
            foreach ($bodyParts as $b){
                $response[] = $b->getName();
                $t = $entityManager->getRepository(Filmler::class)->find($b->getId());

                $entityManager->remove($t);
                $entityManager->flush();
            }
            $io->success("Tebrikler Veritabani silindi..");
        }

       // $entityManager = $this->getDoctrine()->getManager();

        $entityManager =  $this->em;

        $aranan = $input->getOption("search");
        $tvMaze = new Film();

        $array = array_reverse( $tvMaze->apiGetir("http://api.tvmaze.com/search/shows?q=".$aranan ),false);
        $filmCount = count($array);
        $sezonCout =0;
        $bolumCount =0;

        $io->title("Baslatildi...");
        $io->progressStart($filmCount);
        foreach ($array as $item) {
            $film =  $entityManager->getRepository(Filmler::class)->findOneBy(["api_item_id"=>$item->show->id]);

            if(is_null($film)) {
                $film = new Filmler();
                if(!($item->show->network==null)) {
                    $kanal = new  Kanallar();
                    $kanal->setName($item->show->network->name);
                    $kanal->setApiItemId($item->show->network->id);
                    $kanal->setCountryCode($item->show->network->country->code);
                    $kanal->setCountryName($item->show->network->country->name);


                    $entityManager->persist($kanal);
                    $entityManager->flush();

                    $film->setKanal($kanal);
                }

                $film->setRating($item->show->rating->average);
                $film->setName($item->show->name);
                $film->setDay(implode(",", $item->show->schedule->days));
                $film->setScheduleTime($item->show->schedule->time);
                $film->setApiItemId($item->show->id);
                $film->setPremiered(new \DateTime($item->show->premiered));
                $film->setSummary($item->show->summary);
                $film->setSlug($film->getName());


                $entityManager->persist($film);
                $entityManager->flush();
                if(!($item->show->image==null)) {
                    $resim = new Resimler();
                    $resim->setMedium($item->show->image->medium);
                    $resim->setOrginal($item->show->image->original);
                    $resim->setFilmler($film);

                    $entityManager->persist($resim);
                    $entityManager->flush();
                }

            }


            $sezonlar = $tvMaze->apiGetir("http://api.tvmaze.com/shows/".$film->getApiItemId()."/seasons");
            $sezonCout =count($sezonlar);
            foreach ($sezonlar as $sezon_item) {

                $sezon =  $entityManager->getRepository(Sezonlar::class)->findOneBy(["api_item_id"=>$sezon_item->id]);

                if(is_null($sezon)) {

                    $sezon = new Sezonlar();
                    $sezon->setName($sezon_item->name);
                    $sezon->setEndDate(new \DateTime($sezon_item->endDate));
                    $sezon->setApiItemId($sezon_item->id);
                    $sezon->setSummary($sezon_item->summary);
                    $sezon->setPremiereDate(new \DateTime($sezon_item->premiereDate));
                    $sezon->setNumber($sezon_item->number);
                    $sezon->setFilmler($film);
                    $sezon->setSlug($sezon->getName());
                    $entityManager->persist($sezon);
                    $entityManager->flush();
                }

                $bolumler = $tvMaze->apiGetir("http://api.tvmaze.com/seasons/".$sezon->getApiItemId()."/episodes");
                $bolumCount =count($bolumler)+$bolumCount;
                foreach ($bolumler as $bolum_item) {
                    $bolum =  $entityManager->getRepository(Bolumler::class)->findOneBy(["api_item_id"=>$bolum_item->id]);

                    if(is_null($bolum)) {

                        $bolum = new Bolumler();
                        $bolum->setName($bolum_item->name);
                        $bolum->setUrl($bolum_item->url);
                        $bolum->setApiItemId($bolum_item->id);
                        $bolum->setSezonlar($sezon);
                        $bolum->setAirdate(new \DateTime($bolum_item->airdate));
                        $bolum->setAirtime($bolum_item->airtime);
                        $bolum->setSlug($bolum->getName());

                        $entityManager->persist($bolum);
                        $entityManager->flush();
                    }
                }
            }
            $output->writeln(" ");
            $output->writeln(" ");
            $output->writeln($film->getName()." isimli Film, ".$sezonCout." adet sezonu ve ".$bolumCount." adet bolum basariyla kayit edildi.");
       $io->progressAdvance(1);
        }
        $io->progressFinish();
        $io->success("Tebrikler islem basariyla tamamlandi..");
        return 0;

    }



}