<?php

namespace App\Controller;

use App\Entity\Bolumler;
use App\Entity\Filmler;
use App\Entity\Kanallar;
use App\Entity\Resimler;
use App\Entity\Sezonlar;
use App\Service\Film;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\AsciiSlugger;

class AnasayfaController extends AbstractController
{
    /**
     * @Route("/", name="anasayfa")
     * @return Response
     */
    public function index()
    {
        $filmList = $this->getDoctrine()->getRepository(Filmler::class)->findBy(array(), array('id' => 'DESC'),10);;


        return $this->render('index.html.twig', [
            'controller_name' => 'AnasayfaController',
            "filmList"=>$filmList
        ]);

    }

    /**
     * @Route("/test", name="anasayfa-test")
     */
    public function test()//:Response
    {


        $bodyParts = $this->getDoctrine()->getRepository(Filmler::class)->findAll();
        $response = array();
        foreach ($bodyParts as $b){
            $response[] = $b->getName();
            $t = $this->getDoctrine()->getRepository(Filmler::class)->find($b->getId());
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($t);
            $entityManager->flush();
        }
        return new JsonResponse($response);

    }
}
