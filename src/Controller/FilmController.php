<?php

namespace App\Controller;

use App\Entity\Filmler;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FilmController extends AbstractController
{
    /**
     * @Route("/{film_slug}/{film_id}", name="film")
     */
    public function film($film_slug,$film_id): Response
    {
        return $this->redirectToRoute("bolum",["film_slug"=>$film_slug,"film_id"=>$film_id,"sezon"=>1,"bolum"=>1]);
    }
    /**
     * @Route("/{film_slug}/{film_id}/{sezon_id}", name="film")
     */
    public function sezon($film_slug,$film_id,$sezon_id): Response
    {
        return $this->redirectToRoute("bolum",["film_slug"=>$film_slug,"film_id"=>$film_id,"sezon"=>$sezon_id,"bolum"=>1]);
    }
    /**
     * @Route("/{film_slug}/{film_id}/{sezon}/{bolum}", name="bolum")
     */
    public function bolum($film_slug,$film_id,$sezon,$bolum): Response
    {
        $film = $this->getDoctrine()->getRepository(Filmler::class)->find($film_id);
      ;
        if(is_null($film)){
            return $this->redirectToRoute("anasayfa");
        }

        return $this->render('film/index.html.twig', [
            'controller_name' => 'FilmController',
            'film'=>$film,
            'aktifsezon'=>$sezon,
            'aktifbolum'=>$bolum
        ]);
    }

}
