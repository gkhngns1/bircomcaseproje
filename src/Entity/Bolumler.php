<?php

namespace App\Entity;

use App\Repository\BolumlerRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\String\Slugger\AsciiSlugger;

/**
 * @ORM\Entity(repositoryClass=BolumlerRepository::class)
 */
class Bolumler
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=60, nullable=true)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $url;

    /**
     * @ORM\Column(type="integer",unique=true)
     */
    private $api_item_id;

    /**
     * @ORM\ManyToOne(targetEntity=Sezonlar::class, inversedBy="Bolumler")
     *  * @ORM\JoinColumn(name="sezonlar_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $sezonlar;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $slug;

    /**
     * @ORM\Column(type="date" ,nullable=true)
     */
    private $airdate;

    /**
     * @ORM\Column(type="string", length=10,nullable=true)
     */
    private $airtime;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getApiItemId(): ?int
    {
        return $this->api_item_id;
    }

    public function setApiItemId(int $api_item_id): self
    {
        $this->api_item_id = $api_item_id;

        return $this;
    }

    public function getSezonlar(): ?Sezonlar
    {
        return $this->sezonlar;
    }

    public function setSezonlar(?Sezonlar $sezonlar): self
    {
        $this->sezonlar = $sezonlar;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(?string $slug): self
    {
        $this->slug =  (new AsciiSlugger('tr'))->slug($slug." ");;

        return $this;
    }

    public function getAirdate(): ?\DateTimeInterface
    {
        return $this->airdate;
    }

    public function setAirdate(\DateTimeInterface $airdate): self
    {
        $this->airdate = $airdate;

        return $this;
    }

    public function getAirtime(): ?string
    {
        return $this->airtime;
    }

    public function setAirtime(string $airtime): self
    {
        $this->airtime = $airtime;

        return $this;
    }
}
