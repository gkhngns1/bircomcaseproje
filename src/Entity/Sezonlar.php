<?php

namespace App\Entity;

use App\Repository\SezonlarRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\String\Slugger\AsciiSlugger;

/**
 * @ORM\Entity(repositoryClass=SezonlarRepository::class)
 */
class Sezonlar
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=70, nullable=true)
     */
    private $name;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $premiereDate;

    /**
     * @ORM\Column(type="integer")
     */
    private $number;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $endDate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $summary;

    /**
     * @ORM\Column(type="integer" ,unique=true)
     */
    private $api_item_id;

    /**
     * @ORM\ManyToOne(targetEntity=Filmler::class, inversedBy="sezonlar" )
     *  * @ORM\JoinColumn(name="filmler_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $filmler;

    /**
     * @ORM\OneToMany(targetEntity=Bolumler::class, mappedBy="sezonlar")
     */
    private $Bolumler;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $slug;

    public function __construct()
    {
        $this->Bolumler = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPremiereDate(): ?\DateTimeInterface
    {
        return $this->premiereDate;
    }

    public function setPremiereDate(?\DateTimeInterface $premiereDate): self
    {
        $this->premiereDate = $premiereDate;

        return $this;
    }

    public function getNumber(): ?int
    {
        return $this->number;
    }

    public function setNumber(int $number): self
    {
        $this->number = $number;

        return $this;
    }

    public function getEndDate(): ?\DateTimeInterface
    {
        return $this->endDate;
    }

    public function setEndDate(?\DateTimeInterface $endDate): self
    {
        $this->endDate = $endDate;

        return $this;
    }

    public function getSummary(): ?string
    {
        return $this->summary;
    }

    public function setSummary(?string $summary): self
    {
        $this->summary = $summary;

        return $this;
    }

    public function getApiItemId(): ?int
    {
        return $this->api_item_id;
    }

    public function setApiItemId(int $api_item_id): self
    {
        $this->api_item_id = $api_item_id;

        return $this;
    }

    public function getFilmler(): ?Filmler
    {
        return $this->filmler;
    }

    public function setFilmler(?Filmler $filmler): self
    {
        $this->filmler = $filmler;

        return $this;
    }

    /**
     * @return Collection|Bolumler[]
     */
    public function getBolumler(): Collection
    {
        return $this->Bolumler;
    }

    public function addBolumler(Bolumler $bolumler): self
    {
        if (!$this->Bolumler->contains($bolumler)) {
            $this->Bolumler[] = $bolumler;
            $bolumler->setSezonlar($this);
        }

        return $this;
    }

    public function removeBolumler(Bolumler $bolumler): self
    {
        if ($this->Bolumler->removeElement($bolumler)) {
            // set the owning side to null (unless already changed)
            if ($bolumler->getSezonlar() === $this) {
                $bolumler->setSezonlar(null);
            }
        }

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(?string $slug): self
    {
        $this->slug =  (new AsciiSlugger('tr'))->slug($slug." ");;

        return $this;
    }
}
