<?php

namespace App\Entity;

use App\Repository\ResimlerRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ResimlerRepository::class)
 */
class Resimler
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $medium;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $orginal;

    /**
     * @ORM\ManyToOne(targetEntity=Filmler::class, inversedBy="resimler")
     * @ORM\JoinColumn(name="filmler_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $filmler;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMedium(): ?string
    {
        return $this->medium;
    }

    public function setMedium(?string $medium): self
    {
        $this->medium = $medium;

        return $this;
    }

    public function getOrginal(): ?string
    {
        return $this->orginal;
    }

    public function setOrginal(string $orginal): self
    {
        $this->orginal = $orginal;

        return $this;
    }

    public function getFilmler(): ?Filmler
    {
        return $this->filmler;
    }

    public function setFilmler(?Filmler $filmler): self
    {
        $this->filmler = $filmler;

        return $this;
    }
}
