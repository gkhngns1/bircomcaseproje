<?php

namespace App\Entity;

use App\Repository\KanallarRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=KanallarRepository::class)
 */
class Kanallar
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=60, nullable=true)
     */
    private $country_name;

    /**
     * @ORM\Column(type="string", length=60, nullable=true)
     */
    private $country_code;

    /**
     * @ORM\OneToMany(targetEntity=Filmler::class, mappedBy="kanal")
     *  * @ORM\JoinColumn(name="filmler_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $filmlers;

    /**
     * @ORM\Column(type="string", length=60, nullable=true)
     */
    private $name;

    /**
     * @ORM\Column(type="integer")
     */
    private $api_item_id;

    public function __construct()
    {
        $this->filmlers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCountryName(): ?string
    {
        return $this->country_name;
    }

    public function setCountryName(string $country_name): self
    {
        $this->country_name = $country_name;

        return $this;
    }

    public function getCountryCode(): ?string
    {
        return $this->country_code;
    }

    public function setCountryCode(?string $country_code): self
    {
        $this->country_code = $country_code;

        return $this;
    }

    /**
     * @return Collection|Filmler[]
     */
    public function getFilmlers(): Collection
    {
        return $this->filmlers;
    }

    public function addFilmler(Filmler $filmler): self
    {
        if (!$this->filmlers->contains($filmler)) {
            $this->filmlers[] = $filmler;
            $filmler->setKanal($this);
        }

        return $this;
    }

    public function removeFilmler(Filmler $filmler): self
    {
        if ($this->filmlers->removeElement($filmler)) {
            // set the owning side to null (unless already changed)
            if ($filmler->getKanal() === $this) {
                $filmler->setKanal(null);
            }
        }

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getApiItemId(): ?int
    {
        return $this->api_item_id;
    }

    public function setApiItemId(int $api_item_id): self
    {
        $this->api_item_id = $api_item_id;

        return $this;
    }
}
