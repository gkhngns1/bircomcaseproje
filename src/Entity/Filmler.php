<?php

namespace App\Entity;

use App\Repository\FilmlerRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\String\Slugger\AsciiSlugger;

/**
 * @ORM\Entity(repositoryClass=FilmlerRepository::class)
 */
class Filmler
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $schedule_time;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $day;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $rating;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $premiered;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $summary;

    /**
     * @ORM\Column(type="integer", unique=true, options={"unsigned"=true})
     */
    private $api_item_id;

    /**
     * @ORM\OneToMany(targetEntity=Resimler::class, mappedBy="filmler")
     */
    private $resimler;

    /**
     * @ORM\ManyToOne(targetEntity=Kanallar::class, inversedBy="filmlers")
     */
    private $kanal;

    /**
     * @ORM\OneToMany(targetEntity=Sezonlar::class, mappedBy="filmler")
     */
    private $sezonlar;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $slug;

    /**
     * @ORM\Column(type="string", length=100 , nullable=true)
     */
    private $name;

    public function __construct()
    {
        $this->resimler = new ArrayCollection();
        $this->sezonlar = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getScheduleTime(): ?string
    {
        return $this->schedule_time;
    }

    public function setScheduleTime(?string $schedule_time): self
    {
        $this->schedule_time = $schedule_time;

        return $this;
    }

    public function getDay(): ?string
    {
        return $this->day;
    }

    public function setDay(?string $day): self
    {
        $this->day = $day;

        return $this;
    }

    public function getRating(): ?string
    {
        return $this->rating;
    }

    public function setRating(?string $rating): self
    {
        $this->rating = $rating;

        return $this;
    }

    public function getPremiered(): ?\DateTimeInterface
    {
        return $this->premiered;
    }

    public function setPremiered(?\DateTimeInterface $premiered): self
    {
        $this->premiered = $premiered;

        return $this;
    }

    public function getSummary(): ?string
    {
        return $this->summary;
    }

    public function setSummary(?string $summary): self
    {
        $this->summary = $summary;

        return $this;
    }

    public function getApiItemId(): ?int
    {
        return $this->api_item_id;
    }

    public function setApiItemId(int $api_item_id): self
    {
        $this->api_item_id = $api_item_id;

        return $this;
    }

    /**
     * @return Collection|Resimler[]
     */
    public function getResimler(): Collection
    {
        return $this->resimler;
    }

    public function addResimler(Resimler $resimler): self
    {
        if (!$this->resimler->contains($resimler)) {
            $this->resimler[] = $resimler;
            $resimler->setFilmler($this);
        }

        return $this;
    }

    public function removeResimler(Resimler $resimler): self
    {
        if ($this->resimler->removeElement($resimler)) {
            // set the owning side to null (unless already changed)
            if ($resimler->getFilmler() === $this) {
                $resimler->setFilmler(null);
            }
        }

        return $this;
    }

    public function getKanal(): ?Kanallar
    {
        return $this->kanal;
    }

    public function setKanal(?Kanallar $kanal): self
    {
        $this->kanal = $kanal;

        return $this;
    }

    /**
     * @return Collection|Sezonlar[]
     */
    public function getSezonlar(): Collection
    {
        return $this->sezonlar;
    }

    public function addSezonlar(Sezonlar $sezonlar): self
    {
        if (!$this->sezonlar->contains($sezonlar)) {
            $this->sezonlar[] = $sezonlar;
            $sezonlar->setFilmler($this);
        }

        return $this;
    }

    public function removeSezonlar(Sezonlar $sezonlar): self
    {
        if ($this->sezonlar->removeElement($sezonlar)) {
            // set the owning side to null (unless already changed)
            if ($sezonlar->getFilmler() === $this) {
                $sezonlar->setFilmler(null);
            }
        }

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(?string $slug): self
    {
        $this->slug =  (new AsciiSlugger('tr'))->slug($slug." ");;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {

        $this->name = $name;

        return $this;
    }

}
