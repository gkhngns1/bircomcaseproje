<?php

namespace App\Service;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Bolumler;
use App\Entity\Filmler;
use App\Entity\Kanallar;
use App\Entity\Resimler;
use App\Entity\Sezonlar;
use Doctrine\DBAL\Schema\Schema;

class Film{
    public function apiGetir($url){
        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', $url);
        return json_decode($response->getBody());
    }
    public function run()
    {
        $entityManager = $this->getDoctrine()->getManager();
       // $aranan = $input->getOption("search");
        $tvMaze = new Film();

        $array = $tvMaze->apiGetir("http://api.tvmaze.com/search/shows?q=".$aranan );

        foreach ($array as $item) {
            $film = $this->getDoctrine()->getRepository(Filmler::class)->findOneBy(["api_item_id"=>$item->show->id]);

            if(is_null($film)) {


                $kanal = new  Kanallar();
                $kanal->setName($item->show->network->name);
                $kanal->setApiItemId($item->show->network->id);
                $kanal->setCountryCode($item->show->network->country->code);
                $kanal->setCountryCode($item->show->network->country->name);

                $entityManager->persist($kanal);
                $entityManager->flush();

                $film = new Filmler();
                $film->setRating($item->show->rating->average);
                $film->setDay(implode(",", $item->show->schedule->days));
                $film->setScheduleTime($item->show->schedule->time);
                $film->setApiItemId($item->show->id);
                $film->setPremiered(new \DateTime($item->show->premiered));
                $film->setSummary($item->show->summary);
                $film->setSlug($film->getName());
                $film->setKanal($kanal);
                $entityManager->persist($film);
                $entityManager->flush();

                $resim = new Resimler();
                $resim->setMedium($item->image->medium);
                $resim->setOrginal($item->image->original);
                $resim->setFilmler($film);

                $entityManager->persist($resim);
                $entityManager->flush();


            }


            $sezonlar = $tvMaze->apiGetir("http://api.tvmaze.com/shows/".$film->getApiItemId()."/seasons");
            foreach ($sezonlar as $sezon_item) {

                $sezon = $this->getDoctrine()->getRepository(Sezonlar::class)->findOneBy(["api_item_id"=>$sezon_item->id]);

                if(is_null($sezon)) {

                    $sezon = new Sezonlar();
                    $sezon->setName($sezon_item->name);
                    $sezon->setEndDate(new \DateTime($sezon_item->endDate));
                    $sezon->setApiItemId($sezon_item->id);
                    $sezon->setSummary($sezon_item->summary);
                    $sezon->setPremiereDate(new \DateTime($sezon_item->premiereDate));
                    $sezon->setNumber($sezon_item->number);
                    $sezon->setFilmler($film);
                    $sezon->setSlug($sezon->getName());
                    $entityManager->persist($sezon);
                    $entityManager->flush();
                }

                $bolumler = $tvMaze->apiGetir("http://api.tvmaze.com/seasons/".$sezon->getApiItemId()."/episodes");
                foreach ($bolumler as $bolum_item) {
                    $bolum = $this->getDoctrine()->getRepository(Bolumler::class)->findOneBy(["api_item_id"=>$bolum_item->id]);

                    if(is_null($bolum)) {

                        $bolum = new Bolumler();
                        $bolum->setName($bolum_item->name);
                        $bolum->setUrl($bolum_item->url);
                        $bolum->setApiItemId($bolum_item->id);
                        $bolum->setSezonlar($sezon);
                        $bolum->setSlug($bolum->getName());

                        $entityManager->persist($bolum);
                        $entityManager->flush();
                    }
                }
            }
        }
    }
}