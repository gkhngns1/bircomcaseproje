<?php

namespace App\Repository;

use App\Entity\Bolumler;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Bolumler|null find($id, $lockMode = null, $lockVersion = null)
 * @method Bolumler|null findOneBy(array $criteria, array $orderBy = null)
 * @method Bolumler[]    findAll()
 * @method Bolumler[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BolumlerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Bolumler::class);
    }

    // /**
    //  * @return Bolumler[] Returns an array of Bolumler objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Bolumler
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
