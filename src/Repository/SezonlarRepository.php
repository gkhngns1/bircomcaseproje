<?php

namespace App\Repository;

use App\Entity\Sezonlar;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Sezonlar|null find($id, $lockMode = null, $lockVersion = null)
 * @method Sezonlar|null findOneBy(array $criteria, array $orderBy = null)
 * @method Sezonlar[]    findAll()
 * @method Sezonlar[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SezonlarRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Sezonlar::class);
    }

    // /**
    //  * @return Sezonlar[] Returns an array of Sezonlar objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Sezonlar
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
