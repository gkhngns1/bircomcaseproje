<?php

namespace App\Repository;

use App\Entity\Filmler;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Filmler|null find($id, $lockMode = null, $lockVersion = null)
 * @method Filmler|null findOneBy(array $criteria, array $orderBy = null)
 * @method Filmler[]    findAll()
 * @method Filmler[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FilmlerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Filmler::class);
    }

    public function ApiCreateControl(int $api_id){

        return $this->findBy([
            'api_item_id ' => $api_id
        ]);


    }

}
