### Kullanım

- Projeyi clonluyoruz.

```shell
  git clone https://gkhngns1@bitbucket.org/gkhngns1/bircomcaseproje.git
```
- Composer bağıllarını kuruyoruz.
```shell
  composer update

```
- Proje env dosyasını ayarladıktan sonra veritabanını doctrine ile olusturuyoruz.

```shell
php bin\console doctrine:database:create
```
- Veritabanını oluşturduktan sonra ilgili kanallar,filmler,sezonlar,bolumler,resimler tablolarını ve ilişkilerini kurması için migration oluşturup doctrine ile migrationu çalıştırıyoruz.


```shell
php bin\console doctrine:migrations:migrate
```
- Veritabanı tabloları ve ilişkileri hazır olduktan sonra projenin özel tvmaze:insert komutu ile apiden "search" seçeneğine aranacak anahtar kelimeyi giriyoruz. Bu search'a yazılan anahtar kelimeyi http://api.tvmaze.com/search/shows?q={anahtarKelime} apiden gelen filmleri ve bu filmin tüm seonları ile bölümlerini veritabanına kayıt eder.

```shell
php bin\console tvmaze:insert --search="comedy"

```
## Şablon
- Anasayfa'da eklenen son 10 film listelenmektedir.
- Tıklanan filmin son sezonunun son bölümünün detay sayfası açılmaktadır.

## Veritabanı ilişkileri

- Filmler, Resimler, Kanallar, Sezonlar, Bölümler için ayrı ayrı tablolar vardır.
* Bir firmin bir adet kanalı,
* Bir ve birden fazla resmi,
- Bir ve birden fazla sezonu,
- bir ve birden fazla bölümü olabilir.


- Film tablosu resimler tablosuyla (OneToMany)
- Film tablosu kanallar tablosuyla (ManyToOne)
- Film tablosu sezonlar tablosuyla (OneToMany)
- Sezonlar tablosu bölümler tablosuyla (OneToMany) İlişkisi vardır. 


