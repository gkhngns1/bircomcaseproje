<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210307125323 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE bolumler (id INT AUTO_INCREMENT NOT NULL, sezonlar_id INT DEFAULT NULL, name VARCHAR(60) DEFAULT NULL, url VARCHAR(100) NOT NULL, api_item_id INT NOT NULL, slug VARCHAR(100) DEFAULT NULL, airdate DATE DEFAULT NULL, airtime VARCHAR(10) DEFAULT NULL, UNIQUE INDEX UNIQ_EA167655FF512639 (api_item_id), INDEX IDX_EA167655F146327 (sezonlar_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE filmler (id INT AUTO_INCREMENT NOT NULL, kanal_id INT DEFAULT NULL, schedule_time VARCHAR(10) DEFAULT NULL, day VARCHAR(10) DEFAULT NULL, rating VARCHAR(20) DEFAULT NULL, premiered DATE DEFAULT NULL, summary LONGTEXT DEFAULT NULL, api_item_id INT UNSIGNED NOT NULL, slug VARCHAR(100) DEFAULT NULL, name VARCHAR(100) DEFAULT NULL, UNIQUE INDEX UNIQ_2833F017FF512639 (api_item_id), INDEX IDX_2833F017BB3E409B (kanal_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE kanallar (id INT AUTO_INCREMENT NOT NULL, country_name VARCHAR(60) DEFAULT NULL, country_code VARCHAR(60) DEFAULT NULL, name VARCHAR(60) DEFAULT NULL, api_item_id INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE resimler (id INT AUTO_INCREMENT NOT NULL, filmler_id INT DEFAULT NULL, medium VARCHAR(100) DEFAULT NULL, orginal VARCHAR(100) DEFAULT NULL, INDEX IDX_654DD2FDD444F046 (filmler_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE sezonlar (id INT AUTO_INCREMENT NOT NULL, filmler_id INT DEFAULT NULL, name VARCHAR(70) DEFAULT NULL, premiere_date DATE DEFAULT NULL, number INT NOT NULL, end_date DATE DEFAULT NULL, summary VARCHAR(255) DEFAULT NULL, api_item_id INT NOT NULL, slug VARCHAR(100) DEFAULT NULL, UNIQUE INDEX UNIQ_77711CE1FF512639 (api_item_id), INDEX IDX_77711CE1D444F046 (filmler_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE bolumler ADD CONSTRAINT FK_EA167655F146327 FOREIGN KEY (sezonlar_id) REFERENCES sezonlar (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE filmler ADD CONSTRAINT FK_2833F017BB3E409B FOREIGN KEY (kanal_id) REFERENCES kanallar (id)');
        $this->addSql('ALTER TABLE resimler ADD CONSTRAINT FK_654DD2FDD444F046 FOREIGN KEY (filmler_id) REFERENCES filmler (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE sezonlar ADD CONSTRAINT FK_77711CE1D444F046 FOREIGN KEY (filmler_id) REFERENCES filmler (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE resimler DROP FOREIGN KEY FK_654DD2FDD444F046');
        $this->addSql('ALTER TABLE sezonlar DROP FOREIGN KEY FK_77711CE1D444F046');
        $this->addSql('ALTER TABLE filmler DROP FOREIGN KEY FK_2833F017BB3E409B');
        $this->addSql('ALTER TABLE bolumler DROP FOREIGN KEY FK_EA167655F146327');
        $this->addSql('DROP TABLE bolumler');
        $this->addSql('DROP TABLE filmler');
        $this->addSql('DROP TABLE kanallar');
        $this->addSql('DROP TABLE resimler');
        $this->addSql('DROP TABLE sezonlar');
    }
}
